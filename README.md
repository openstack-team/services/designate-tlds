This package fill-up the Designate database with the global TLDs list
downloaded from Mozilla. For doing so, designate-tlds will download
the TLD list from publicsuffix.org (the default is to take the list
from https://publicsuffix.org/list/public_suffix_list.dat, however,
any URL that respect the same format will do), parse it, and call
the Designate API to add the missing TLDs.

By default, designate-tlds is setup to run every week, so it maintains
the list of TLDs up-to-date.
