#!/bin/sh

set -e

oslo-config-generator --output-file designate-tlds.conf \
  --namespace desigtldsopts \
  --namespace oslo.log \
  --namespace keystonemiddleware.auth_token
